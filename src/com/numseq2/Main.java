package com.numseq2;

import java.math.BigDecimal;

public class Main {
    static double silnia(double n) {
        if (n == 1)
            return 1;
        else
            return n * silnia(n - 1);
    }

    static BigDecimal euler() {
        BigDecimal suma = new BigDecimal("1");
        for (int i = 1; i < 1000; i++) {
            BigDecimal wyrazenie = BigDecimal.valueOf((1 / silnia(i)));
            suma = suma.add(wyrazenie);
        }
        return suma;
    }

    static boolean czyPierwsza(long num) {
        if (num < 2)         //liczba mniejsza od 2 nie moze byc liczba pierwsza
            return false;
        for (long i = 2; i < num / 2; i++) {    //sprawdzamy dzielnik (i), zaczynamy od dwójki, gdyż wszystkie liczby są podzielne przez 1
            if (num % i == 0)
                return false;
        }
        return true;
    }

    static void znajdzDziesieciocyfrowaLiczbe(BigDecimal e, int x) {
        String str = String.valueOf(e);
        String wycinek = str.substring(x, x + 10);   //tworzymy wycinek 10 cyfr
        long dziesieciocyfrowaLiczba = Long.parseLong(wycinek);
        if (czyPierwsza(dziesieciocyfrowaLiczba)) {
            System.out.println("Znalazlem liczbe pierwsza = " + dziesieciocyfrowaLiczba);
            return;
        }
        else
            znajdzDziesieciocyfrowaLiczbe(e, x + 1);  //jezeli liczba nie zostala znaleziona rekurencyjnie przesuwamy sie o jeden w prawo
    }

    public static void main(String[] args) {
        System.out.println(euler());
        znajdzDziesieciocyfrowaLiczbe(euler(), 2);
    }
}
